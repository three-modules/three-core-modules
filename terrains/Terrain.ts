import * as THREE from "three";
import { Material, Mesh, PlaneGeometry, Vector3 } from "three";
import { mergeBufferGeometries } from "three/examples/jsm/utils/BufferGeometryUtils";

export enum Alignment {
  Center,
  TopLeft,
  BottomLeft,
}

/**
 * Extends PlaneGeomtry to add terrain generation features
 * It can be used as a renderer for an heightmap in 3d
 */
export class TerrainGeometry extends PlaneGeometry {
  // resolution or definition
  rows;
  cols;
  // dimensions
  // scale: Vector3;
  width;
  height;
  // data source
  datasource;
  // rendering
  // geom: THREE.PlaneGeometry;
  mesh: THREE.Mesh;
  renderSplits;
  lastBatch;

  constructor(width, height, cols, rows, datasource) {
    super(width, height, cols - 1, rows - 1);
    // this.heightmap = heightmap; //new Heightmap(cols, rows);
    this.datasource = datasource;
    this.rows = rows;
    this.cols = cols;
    // this.scale = scale;
    this.width = width; //rows * this.scale.x;
    this.height = height; //cols * this.scale.z;

    this.rotateX(-Math.PI / 2);
  }

  // build geom, create mesh,
  render(t?) {
    // console.log(this.rows);
    let batch = [...Array(this.rows).keys()];
    this.renderBatch(batch, t);
    this.computeVertexNormals();
    // this.geom.attributes.position.needsUpdate = true;

    // console.log("[Terrain] done rendering ");
    // return this.mesh;
  }

  renderBatch(batch, t?) {
    // console.log("render batch rows: " + batch);
    batch.forEach((row) => {
      this.renderRow(row, t);
    });
    this.attributes.position.needsUpdate = true;
  }

  renderRow(row, t?) {
    // console.log(" rows");
    var vertices = this.attributes.position;
    // var uvs = this.geom.attributes.uv.array;
    const arr: any = [...Array(this.cols).keys()];
    arr.forEach(async (col) => {
      // const height = this.heightmap.getHeightAt(row, col);
      const index = row * this.rows + col;
      const x = vertices.getX(index);
      const z = vertices.getZ(index);
      const y = this.datasource(col / this.cols, row / this.rows, t);
      vertices.setY(index, y); //await this.datasource(col, row);
      vertices[index * 3 + 1] = y //* this.scale.y;
    });
  }

  align(point, alignment) {
    switch (alignment) {
      case Alignment.BottomLeft:
        break;
      default:
        break;
    }
  }

  // dynamic terrain
  update(time) {
    // var T = Math.round(time);
    var Tcos = Math.cos(time / 10);
    // if (this.lastBatch)
    //     this.render(Tcos);
  }
}

/**
 * Voxel blocks or minecraft version
 */
export class VoxelBlocksGeometry extends TerrainGeometry {
  blkSize;
  blkGeoms;

  constructor(cols, rows, blksize, datasource) {
    super(blksize * cols, blksize * rows, cols, rows, datasource);
    console.log(`[BlkTerrain] block size: ${blksize} m`);
    this.blkSize = blksize;
    this.blkGeoms = initBlkGeom(this.blkSize);
  }

  // renderRow(row) {
  //     let rowBlkGeoms = [];
  //     const cols: number = this.heightmap.width;
  //     var vertices = this.geom.attributes.position.array;
  //     // var uvs = this.geom.attributes.uv.array;

  //     [...Array(cols).keys()].forEach(async col => {
  //         // const height = this.heightmap.getHeightAt(row, col);
  //         const blkGeom: any = this.buildBlock(col, row);
  //         rowBlkGeoms.push(...blkGeom);
  //     });
  // }

  // override the render function
  render(t?) {
    const arr: any = [...Array(this.rows).keys()];
    const blocksGeoms: any = arr.map((row) => {
      const arr2: any = [...Array(this.cols).keys()];
      const rowBlocks = arr2.map((col) => {
        const blk: any = this.buildBlock(col, row);
        return blk;
      });
      return rowBlocks;
      // rowBlkGeom = this.renderRow(row);
    });

    // (this.geometry as any) = mergeBufferGeometries(blocksGeoms.flat(2));
    this.computeBoundingSphere();
  }

  /**
   * Build single block
   * @returns
   */
  buildBlock(x, z) {
    const geometries = [];
    const matrix = new THREE.Matrix4();
    const inc = 1; //this.blkSize;
    // const heightmap = this.heightmap as GeneratedHeightmap;

    // let h = this.heightmap.getHeight(x, z);
    // const y = Math.round(h / 64);
    // console.log("blocks")

    // look at neighboors for optimal geometry
    // const px = this.heightmap.getHeight(x + inc, z);
    // const nx = this.heightmap.getHeight(x - inc, z);
    // const pz = this.heightmap.getHeight(x, z + inc);
    // const nz = this.heightmap.getHeight(x, z - inc);
    const pts = [
      [x, z],
      [x + inc, z],
      [x - inc, z],
      [x, z + inc],
      [x, z - inc],
    ];

    const [y, px, nx, pz, nz] = pts
      .map((c) => this.datasource(c[0], c[1]))
      .map((h) => Math.round(h / 8));

    matrix.makeTranslation(
      x * this.blkSize, // - terrainWidth / 2,
      y * this.blkSize,
      z * this.blkSize //- terrainDepth / 2
    );

    geometries.push(this.blkGeoms.py.clone().applyMatrix4(matrix));

    if ((px !== y && px !== y + 1) || x === 0) {
      geometries.push(this.blkGeoms.px.clone().applyMatrix4(matrix));
    }

    if ((nx !== y && nx !== y + 1) || x === this.cols - 1) {
      geometries.push(this.blkGeoms.nx.clone().applyMatrix4(matrix));
    }

    if ((pz !== y && pz !== y + 1) || z === this.rows - 1) {
      geometries.push(this.blkGeoms.pz.clone().applyMatrix4(matrix));
    }

    if ((nz !== y && nz !== y + 1) || z === 0) {
      geometries.push(this.blkGeoms.nz.clone().applyMatrix4(matrix));
    }

    return geometries;
  }
}

const initBlkGeom = (blockSize) => {
  const blockGeoms = {
    px: new PlaneGeometry(blockSize, blockSize),
    nx: new PlaneGeometry(blockSize, blockSize),
    py: new PlaneGeometry(blockSize, blockSize),
    pz: new PlaneGeometry(blockSize, blockSize),
    nz: new PlaneGeometry(blockSize, blockSize),
  };

  let uvattr: any;
  uvattr = blockGeoms.px.attributes.uv.array[1];
  uvattr = 0.5;
  uvattr = blockGeoms.px.attributes.uv.array[3];
  uvattr = 0.5;
  blockGeoms.px.rotateY(Math.PI / 2);
  blockGeoms.px.translate(blockSize / 2, 0, 0);

  uvattr = blockGeoms.nx.attributes.uv.array[1];
  uvattr = 0.5;
  uvattr = blockGeoms.nx.attributes.uv.array[3];
  uvattr = 0.5;
  blockGeoms.nx.rotateY(-Math.PI / 2);
  blockGeoms.nx.translate(-blockSize / 2, 0, 0);

  uvattr = blockGeoms.py.attributes.uv.array[5];
  uvattr = 0.5;
  uvattr = blockGeoms.py.attributes.uv.array[7];
  uvattr = 0.5;
  blockGeoms.py.rotateX(-Math.PI / 2);
  blockGeoms.py.translate(0, blockSize / 2, 0);

  uvattr = blockGeoms.pz.attributes.uv.array[1];
  uvattr = 0.5;
  uvattr = blockGeoms.pz.attributes.uv.array[3];
  uvattr = 0.5;
  blockGeoms.pz.translate(0, 0, blockSize / 2);

  uvattr = blockGeoms.nz.attributes.uv.array[1];
  uvattr = 0.5;
  uvattr = blockGeoms.nz.attributes.uv.array[3];
  uvattr = 0.5;
  blockGeoms.nz.rotateY(Math.PI);
  blockGeoms.nz.translate(0, 0, -blockSize / 2);

  return blockGeoms;
};
