/**
 * The heighmap class
 */

import { Vector3 } from "three";

// import { TextureData } from "./TextureUtils";
// import * as THREE from "three";

/**
 * Heightmap data can take from either datasources or generator.
 * Both returns a value for a given input, but there are some differences
 * - generator are computational while datasources are stored values
 * - generator are synchroneous while datasources are usualy asynch (remote API, file or DB I/O, )
 * An heightmap can be used in 2 modes:
 * - access from inner storage : either previously generated or fetched data
 * - on the fly data fetch or generation (particularly convenient for async datasources); 
 * the heightmap is built progressively
 * 
 * get => sync access from previously stored val or generate
 * generate => sync generation
 * fetch => datasource
 * fill => async fetching 
 * 
 */
export class Heightmap {
  width;
  height;
  data: Uint8Array;
  generator;
  datasource;

  // texData: TextureData;

  // func: any;

  constructor(width, height) {
    this.width = width;
    this.height = height;
    // this.func = func;
    // console.log(func);
    this.data = new Uint8Array(width * height);
    // this.texData = new TextureData(width);
  }

  // get value stored in heightmap or directly from generator
  getHeight(row, col, onthefly = false) {
    // bypass stored data to get fresh data 
    // if (onthefly) {
    //   let val = this.datasource.get(col, row);
    //   this.setHeightAt(row, col, val);
    // }
    let val = this.data[row * this.width + col];
    // val = (val !== undefined && val !== null) ? val : this.generator(row, col);
    if (val === undefined || val === null) {
      console.log("onthefly")
      val = this.generator(row, col);
      this.setHeightAt(row, col, val);
    }
    return val;
  }

  setHeightAt(row, col, val) {
    this.data[row * this.width + col] = val;
  }

  // TODO support args like time to forward to generator
  generate() {
    // for (let i = 0; i < this.data.length; i++) {
    //   const x = i % this.width, y = (i / this.width) | 0;
    //   this.data[i] = this.generator(x, y, 0);
    // }
    let val;
    for (var row = 0; row < this.height; row++) {
      // row
      for (var col = 0; col < this.width; col++) {
        // col
        val = this.generator(col, row, 0);
        this.setHeightAt(row, col, val);
      }
    }
  }

  /**
  * async version
  */
  async fill(asyncFunc) {
    console.log("fillFromAsyncFunc");
    let val;
    for await (const dummy of [...new Array(256)].map(async (dum, row) => {
      // console.log("row");

      for await (const dummy2 of [...new Array(256)].map(async (dum2, col) => {
        val = await asyncFunc(row, col);
        this.setHeightAt(row, col, val);
        // this.tiles[rowKey][colKey]=tileImg;
      })) {
        // console.log(tileObj)
      }
    }));
  }

  /**
* Fetch data from async datasource like an api server 
* @param row 
* @param col 
*/
  async onTheFly(row, col) {
    const val = this.getHeight(row, col, true);
    // const val = await this.datasource.fetch({ x: col, y: row });
    this.setHeightAt(row, col, val);
    return val;
  }

  /**
   * 
   * @param inputData a set of points to interpolate from
   */
  interpolateFrom(inputData: [Vector3]) {

  }

  render() {

  }

  // getTexture(){
  //   this.texData.fillTexture(this.data);
  //   new THREE.DataTexture(rawData, size, size, THREE.RGBFormat)
  // }
}