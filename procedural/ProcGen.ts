import { SimplexNoise } from "three/examples/jsm/math/SimplexNoise";
import { Heightmap } from "../terrains/Heightmap";

const generateHeight = (perlin, heightmap: Heightmap) => {
  const data = [],
    size = heightmap.width * heightmap.height,
    z = Math.random() * 100;

  let quality = 2;

  for (let j = 0; j < 4; j++) {
    // if (j === 0) for (let i = 0; i < size; i++) heightmap.data[i] = 0;

    for (let i = 0; i < size; i++) {
      const x = i % heightmap.width,
        y = (i / heightmap.width) | 0;
      heightmap.data[i] += perlin.noise(x / quality, y / quality, z) * quality;
    }

    quality *= 4;
  }
  // return data;
};

const noiseGen = (x, y, z, simplex: SimplexNoise) => {
  let quality = 2;
  let noise = 0;
  let val = 0

  for (let j = 0; j < 4; j++) {
    val = simplex.noise3d(x / quality, y / quality, z)
    noise += val * quality;
    quality *= 4;
  }
  return noise;
};

export { noiseGen };
