import * as THREE from "three";
import { Mesh, Vector3 } from "three";


let mat: any = new THREE.MeshPhongMaterial({
    color: 0xaaaaaa,
    specular: 0xffffff,
    shininess: 250,
    side: THREE.DoubleSide,
    vertexColors: true,
  });
  
  mat = new THREE.MeshBasicMaterial({ color: 0xff0000, side: THREE.DoubleSide });
  
  export class HighlightTriangle extends Mesh {
    center;
  
    constructor() {
      super(new THREE.BufferGeometry(), mat);
  
      const positions = new Float32Array(1 * 3 * 3);
      const normals = new Float32Array(1 * 3 * 3);
      const colors = new Float32Array(1 * 3 * 3);
  
      this.geometry.setAttribute(
        "position",
        new THREE.BufferAttribute(positions, 3)
      );
      // this.geometry.setAttribute('normal', new THREE.BufferAttribute(normals, 3));
      this.geometry.setAttribute("color", new THREE.BufferAttribute(colors, 3));
  
      const pos = new Vector3(0, 0, 0);
  
      this.update(pos, pos, pos);
  
      this.geometry.computeBoundingSphere();
      this.renderOrder = 999;
      this.onBeforeRender = function (renderer) {
        renderer.clearDepth();
      };
    }
  
    update(a: Vector3, b: Vector3, c: Vector3) {
      // positions
      const positionAttr = this.geometry.getAttribute("position");
  
      const toto = [a, b, c].forEach((elt, i) => {
        positionAttr.setXYZ(i, elt.x, elt.y, elt.z);
      });
  
      positionAttr.needsUpdate = true;
  
      // colors
      const colorAttr = this.geometry.getAttribute("color");
  
      const color = new THREE.Color();
      color.setRGB(1, 0, 0);
  
      const colors = [color, color, color];
  
      colors.forEach((col, i) => colorAttr.setXYZ(i, col.r, col.g, col.b));
  
      // todo compute more accurate center
      this.center = a;
    }
  }