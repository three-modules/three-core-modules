import * as THREE from "three";

import * as CANNON from "cannon-es";
import { Vec3 } from "cannon-es";
import { SpotLight, Vector3 } from "three";

export enum CONSTRAINT_MODES {
  WALKING,
  DRAGGING,
  FREE,
}

export class Player extends THREE.Group {
  static instances = [];
  static current: Player;
  appInstance;
  config;
  modelClass: any;
  prevAction: THREE.AnimationAction;
  currAction: THREE.AnimationAction;
  currActionName;
  actionMapping;
  body: CANNON.Body;
  anchorBody: CANNON.Body;
  walkConstraint: CANNON.Constraint;
  spiderConstraint: CANNON.Constraint;
  constraintMode = CONSTRAINT_MODES.WALKING;
  groundContact = false;
  spotLight: SpotLight;

  constructor(modelClass, appInstance) {
    super();
    this.appInstance = appInstance;
    // const modelClass: ModelsFactory = (props.modelClass as ModelsFactory).singleton()
    // ModelsFactory.create();
    console.log("[Player] new instance");
    // this.config = props
    this.modelClass = modelClass;
    this.add(modelClass.model);
    // if(!Player.model) Player.loadModel(this.config.model)
    this.actionMapping = this.modelClass.actionMapping;
    this.currActionName = this.actionMapping.idle;
    this.currAction = this.modelClass.actions[this.currActionName];
    this.fadeToAction(this.actionMapping.run, 0.5, 1);
    Player.instances.push(this);
    Player.current = this;
  }

  /**
   * physic's body
   */
  initBody() {
    console.log("[Character] Init body");
    const body = new CANNON.Body({ mass: 100 });
    body.addShape(
      new CANNON.Box(new CANNON.Vec3(2, 9, 1)),
      new CANNON.Vec3(0, 9, 0)
    ); // head,
    // body.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), -Math.PI / 2)
    body.position.x = this.modelClass.model.position.x;
    body.position.y = this.modelClass.model.position.y + 100;
    body.position.z = this.modelClass.model.position.z;
    body.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), 0.15);
    body.addEventListener("collide", (e: any) => {
      const impactVelocity = e.contact.getImpactVelocityAlongNormal(); //e.contact.impactVelocity
      console.log("collision intensity: " + impactVelocity);
      if (this.constraintMode === CONSTRAINT_MODES.FREE) this.walkingMode();
      this.groundContact = true;
    });
    this.appInstance.world.addBody(body);
    this.body = body;
    // Spider drag body
  }

  /**
   * constraints
   */
  initConstraints() {
    // Dragging anchor body
    const bodyShape = new CANNON.Sphere(1);
    const anchorBody = new CANNON.Body({ mass: 0 });
    anchorBody.addShape(bodyShape);
    anchorBody.collisionFilterGroup = 0;
    anchorBody.collisionFilterMask = 0;
    anchorBody.position.set(0, 10, 0);
    this.anchorBody = anchorBody;
    this.appInstance.world.addBody(this.anchorBody);

    const constraintForce = 100;

    // walk
    this.walkConstraint = new CANNON.PointToPointConstraint(
      this.body,
      new CANNON.Vec3(0, 0, 0),
      this.anchorBody,
      new CANNON.Vec3(0, 0, 0),
      constraintForce * 2
    );
    this.appInstance.world.addConstraint(this.walkConstraint);

    // drag
    const jointConstraint = new CANNON.PointToPointConstraint(
      this.body,
      new CANNON.Vec3(0, 18, 0),
      this.anchorBody,
      new CANNON.Vec3(0, 0, 0),
      constraintForce
    );
    this.spiderConstraint = jointConstraint;
    this.appInstance.world.addConstraint(this.spiderConstraint);

    this.appInstance.world.gravity.set(0, -10, 0);
    // this.toggleConstraintMode(CONSTRAINT_MODES.WALKING);
    this.releaseConstraints();
  }

  // Attach spotlight to player
  initSpotLight() {
    const spot = new THREE.SpotLight();
    spot.position.set(0, 10, 0);
    // spot.lookAt(0, 0, this.position.z + 50);
    spot.castShadow = true;
    spot.intensity = 100;
    this.add(spot);
    this.spotLight = spot;
  }

  trigElastic(target) {
    if (this.constraintMode !== CONSTRAINT_MODES.DRAGGING) {
      console.log("dragging mode => target:");
      console.log(target);
      this.constraintMode = CONSTRAINT_MODES.DRAGGING;
      this.walkConstraint.disable();
      // update anchor pos depending on target
      this.anchorBody.position.x = target.x;
      this.anchorBody.position.y = target.y;
      this.anchorBody.position.z = target.z;
      this.body.linearDamping = 0;
      this.body.angularDamping = 0;
      this.spiderConstraint.enable();
      this.groundContact = false;
    }
  }

  releaseConstraints() {
    console.log("constraints released");
    this.constraintMode = CONSTRAINT_MODES.FREE;
    this.spiderConstraint.disable();
    this.walkConstraint.disable();
    this.body.linearDamping = 0;
    this.body.angularDamping = 0.4;
  }

  walkingMode() {
    console.log("walking mode");
    this.constraintMode = CONSTRAINT_MODES.WALKING;
    this.spiderConstraint.disable();
    this.anchorBody.position.copy(this.body.position);
    this.body.linearDamping = 0.8;
    this.body.angularDamping = 0.5;
    this.walkConstraint.enable();
  }

  jump() {
    if (this.constraintMode === CONSTRAINT_MODES.WALKING) {
      console.log("jumping");
      // if (this.constraintMode !== CONSTRAINT_MODES.FREE)
      //   this.releaseConstraints();

      // this.body.applyForce(new Vec3(0, 10000, 0));
      this.body.applyImpulse(new Vec3(0, 10000, 0));
      this.groundContact = false;
    }
  }

  fire() {
    console.log("firing");
  }

  move = (dt, move) => {
    if (!(move instanceof THREE.Vector3)) return;
    if (move.x === 0 && move.z === 0) return;
    const pos = this.anchorBody.position;
    const movement = new Vec3(move.x, move.y, move.z);
    const nextPos = pos.vadd(movement);

    const angle = Math.PI + Math.atan2(pos.x - nextPos.x, pos.z - nextPos.z);

    // this.position.copy(nextPosition)
    pos.set(nextPos.x, 1, nextPos.z);

    // if (this.model)
    //     this.model.setRotationFromAxisAngle(new THREE.Vector3(0, 1, 0), angle)
    this.body.quaternion.setFromAxisAngle(new Vec3(0, 1, 0), angle);
  };

  fadeToAction = (name, duration, speed) => {
    console.log("switch animation to " + name + " at speed: " + speed);

    this.prevAction = this.currAction;
    this.currActionName = name;
    this.currAction = this.modelClass.actions[this.currActionName];

    if (this.prevAction !== this.currAction) this.prevAction.fadeOut(duration);
    const timeScale = name === "Walking" ? 8 * speed : 3 * speed;

    this.currAction
      .reset()
      .setEffectiveTimeScale(timeScale)
      .setEffectiveWeight(1)
      .fadeIn(duration)
      .play();
  };

  animate = (dt, move: Vector3) => {
    const speed = move.length();

    let action = this.actionMapping.idle;

    if (!speed) action = this.actionMapping.idle;
    else
      action = speed < 0.4 ? this.actionMapping.walk : this.actionMapping.run;

    // this.setAction(action)
    if (action !== this.currActionName) this.fadeToAction(action, 0.5, speed);
    this.modelClass.mixer.update(dt);
  };

  update = (dt, move) => {
    if (!this.currAction) return;
    if (this.groundContact) this.animate(dt, move); // only allow anim on the ground
    this.move(dt, move);

    // sync mesh with body
    this.position.set(
      this.body.position.x,
      this.body.position.y,
      this.body.position.z
    );
    this.modelClass.model.quaternion.set(
      this.body.quaternion.x,
      this.body.quaternion.y,
      this.body.quaternion.z,
      this.body.quaternion.w
    );
    // update spot light
    // this.spotLight?.quaternion.copy(this.quaternion);
    // this.spotLight?.lookAt(this.spotLight.position.x + 50, 0, 0);
  };
}
