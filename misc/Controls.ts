import * as THREE from "three";

export const coef = .4

export class OrbitalCamera extends THREE.Object3D {
  originalCameraAngle;
  lastRot = { x: 0, y: 0 };
  camera: THREE.PerspectiveCamera;
  constructor(fov, aspect, near, far) {
    super();

    this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    this.camera.position.set(2, 10, 25);
    this.camera.lookAt(0, 0, -10);
    this.add(this.camera);
    this.originalCameraAngle = this.camera.rotation.x;
  }

  update(rotation) {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    // EULER: x: vertical, y: horizontal
    if (rotation instanceof THREE.Euler) {
      // horizontal rotation
      const quat = this.quaternion.clone();
      const rotY = rotation.y 
      quat.setFromAxisAngle(new THREE.Vector3(0, 1, 0), -rotY);
      this.quaternion.slerp(quat, 0.1);

      // vertical position of camera depending on vertical rotation
      const newCamPos = this.camera.position.clone();
      newCamPos.setY(25 - rotation.x * 11);
      // this.camera.position.lerp(newCamPos, .1)

      const newCamQuat = this.camera.quaternion.clone();
      newCamQuat.setFromAxisAngle(
        new THREE.Vector3(1, 0, 0),
        this.originalCameraAngle +
          rotation.x * coef
      );
      this.camera.quaternion.slerp(newCamQuat, 0.1);
    }

    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
  }
}