import { Clock } from "three";
import { FirstPersonControls } from "three/examples/jsm/controls/FirstPersonControls";

export enum CONTROLS {
    FirstPerson
    // Orbit: OrbitControls
}

export class ThreeEngine {
    threeInstance;
    controls;
    motionBindings = [];

    constructor(threeInstance) {
    console.log("[ThreeEngine] instanciation");
        this.threeInstance = threeInstance;
    }

    update(delta) {
        this.motionBindings.forEach(mb => mb.update(this.threeInstance.clock))
        if (this.controls) this.controls.update(delta);
    }

    setControls(controlType: CONTROLS) {
        switch (controlType) {
            case CONTROLS.FirstPerson:
                this.controls = new FirstPersonControls(this.threeInstance.camera, this.threeInstance.gl.domElement);
                this.controls.movementSpeed = 10;
                this.controls.lookSpeed = 0.125;
                this.controls.lookVertical = true;
                break;
            default:
            // todo
        }
    }

    bindMotion(object, motion) {
        this.motionBindings.push(new MotionBinder(object, motion));
    };
}



export class MotionBinder {
    object;
    motion;
    constructor(object, motionPattern) {
        this.object = object;
        this.motion = motionPattern;
    }

    update(time: Clock) {
        this.motion(this.object.position, time.elapsedTime);
    }
}