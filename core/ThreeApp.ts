import * as THREE from "three";
import * as CANNON from "cannon-es";
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer";
import Stats from "three/examples/jsm/libs/stats.module";
import CannonDebugRenderer from "../external/cannonDebug";
// import { DEFAULT_KEYBINDINGS } from './config'
import { Group } from "three";

/**
 * Three app skeleton
 */
export class ThreeApp {
  // the singleton instance => should be private as well as the constructor
  static instances = [];
  // Three vitals
  renderer;
  scene;
  camera;

  world = new CANNON.World();

  controls: any = {};

  clock = new THREE.Clock();
  delta;
  time;

  /**
   * Instanciate a new class
   */
  static instanciate(appClass, canvasRef, ...params) {
    // if (!ThreeApp.instance) {
    console.log("[ThreeAPP] create new instance for " + appClass.Name);
    const instance = new appClass(params);
    const canvas = canvasRef.current;
    instance.renderer = new THREE.WebGLRenderer({ canvas });
    instance.scene = new THREE.Scene();
    window.addEventListener("resize", instance.onWindowResize, false);
    instance.init();

    // }
    this.instances.push(instance);
    return instance;
  }

  defaults() {
    console.log("[ThreeApp] defaults");

    this.camera = new THREE.PerspectiveCamera(
      45,
      window.innerWidth / window.innerHeight,
      0.1,
      1000
    );

    // params.ref.current.appendChild(this.renderer.domElement);
    this.renderer.autoClear = true;
    // this.renderer.setClearColor(0xff0000)
    this.renderer.physicallyCorrectLights = true;
    this.renderer.outputEncoding = THREE.sRGBEncoding;
    // this.renderer.toneMapping = THREE.ReinhardToneMapping;
    // this.renderer.toneMappingExposure = 6;
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.autoUpdate = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    // renderer.setSize(window.innerWidth, window.innerHeight)
    // renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
    this.scene.background = new THREE.Color(0x000000);
    this.camera.position.set(0, 0, 0);
    this.world.gravity.set(0, -10, 0);
    (this.world as any).solver.iterations = 20;
  }

  onWindowResize() {
    console.log("[Events] resize");
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
  }

  update() {
    this.delta = Math.min(this.clock.getDelta(), 0.1);
    this.time = this.clock.getElapsedTime();
    this.world.step(this.delta);
    //if (this.state.debug) this.cannonDebugRenderer.update()
  }

  render() {
    this.renderer.render(this.scene, this.camera);
  }
}

/**
 * Add native features:
 * - orbit controls
 * - postprocessing effects
 * - raycasting
 *
 */
export class AdvancedThreeApp extends ThreeApp {
  composer: EffectComposer;

  // Debug tools
  stats = Stats();
  cannonDebugRenderer = new CannonDebugRenderer(this.scene, this.world);
  raycaster = new THREE.Raycaster();
  pointer = new THREE.Vector2(0, 0);

  init(params?): void {
    super.defaults();
    console.log("[AdvThreeApp] init");
    document.addEventListener("pointermove", this.onPointerMove);
    this.scene.background = new THREE.Color(0xefd1b5);
    this.scene.fog = new THREE.FogExp2(0xefd1b5, 0.002);
  }

  initComposer = () => {
    // POST PROCESSING
    const renderTarget = new THREE.WebGLRenderTarget(800, 600, {
      minFilter: THREE.LinearFilter,
      magFilter: THREE.LinearFilter,
      format: THREE.RGBAFormat,
      encoding: THREE.sRGBEncoding,
    });

    this.composer = new EffectComposer(this.renderer, renderTarget);
    this.composer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
    this.composer.setSize(window.innerWidth, window.innerHeight);
  };

  onPointerMove = (event) => {
    // this.pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
    // this.pointer.y = -(event.clientY / window.innerHeight) * 2 + 1;
  };

  onWindowResize() {
    super.onWindowResize();
    // Update effect composer
    if (this.composer) {
      this.composer.setSize(window.innerWidth, window.innerHeight);
      this.composer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
    }
  }

  update() {
    super.update();
    this.stats.update();

    this.raycaster.setFromCamera(this.pointer, this.camera);
  }

  render() {
    if (this.composer) this.composer.render();
    else super.render();
  }
}

/**
 * Building on top of threeapp vitals, to expose additional fields/features
 */
export class ThreeDemoApp extends AdvancedThreeApp {
  state: any = {};
  // additional exposed fields
  keyControl;
  debugTools = new Group();

  // init(): void {
  //     super.init()
  // }

  initDebugTools(gridSize?, gridDef?) {
    this.state.debug = false;

    this.scene.add(this.debugTools);
    gridDef = gridDef ? gridDef : 32;
    gridSize = gridSize ? gridSize : 512;
    const grid = new THREE.GridHelper(gridSize, gridDef);
    grid.receiveShadow = true;
    this.debugTools.add(grid);

    const axesHelper = new THREE.AxesHelper(50);
    this.debugTools.add(axesHelper);
  }

  toggleDebug() {
    this.debugTools.visible = !this.debugTools.visible;
  }

  testKeyBindings() {
    Object.keys(this.controls.keys)
      .filter(
        (key) => !isNaN(this.controls.keys[key]) && this.controls.keys[key]
      )
      .forEach((keyBind) => console.log(keyBind));
  }

  initControls(customBindings = {}) {
    // Key bindings
    // overried default with custom if any
    // const keyBindings = { ...DEFAULT_KEYBINDINGS, ...customBindings }
    // this.controls.keys = new KeyInput(keyBindings, KEY_INPUT_TYPE.TRIGGER)
    this.controls.joyLeft = { x: 0, y: 0 };
    this.controls.joyRight = { x: 0, y: 0 };
  }

  update = () => {
    super.update();
    if (this.debugTools.visible && false) this.cannonDebugRenderer.update();
  };
}
