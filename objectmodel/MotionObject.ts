export class MotionObject {
    mesh;
    body;

    init(body, mesh){
        this.body = body
        this.mesh = mesh
    }

    update(time){
        this.mesh.position.set(this.body.position.x, this.body.position.y, this.body.position.z)
        this.mesh.quaternion.set(this.body.quaternion.x, this.body.quaternion.x, this.body.quaternion.y, this.body.quaternion.z)
    }
}