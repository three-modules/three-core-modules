import * as THREE from 'three'

/**
 * A class to handle models
 */

export class Model extends THREE.Group {
    static model
    static instances = []
    static current; // the active model

    constructor(){
        super()
        Model.instances.push(this)
        this.add(Model.model)
    }
}