# three-core-modules

Set of reusable classes/modules for threejs

pure TS files with THREE.JS as main dep 

## Modules:
- core: three app skeleton
- objectmodel:  physic object, model classes ...
- terrains
- procedural
- voxels