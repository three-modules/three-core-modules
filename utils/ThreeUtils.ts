import * as THREE from 'three'

/**
 * promise from three loader
 * @param url 
 * @returns 
 */
export const loadPromise = async (asset, loader) => {
    return new Promise(resolve => {
        loader.load(asset, resolve,
            (xhr) => {
                console.log((xhr.loaded / xhr.total) * 100 + '% loaded')
            },
            (error) => {
                console.log(error)
            });
    });
}