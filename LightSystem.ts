import * as THREE from "three";
import { DirectionalLight } from "three";

export class LightSystem {

    static sunlight() {
        const dirlight = new DirectionalLight(0xFEE465, 1);
        dirlight.name = "SunLight";
        dirlight.position.set(700, 200, 0); 			//default; light shining from top
        dirlight.castShadow = true;            // default false

        //Set up shadow properties for the light
        dirlight.shadow.mapSize.width = 1024;  // default
        dirlight.shadow.mapSize.height = 1024; // default
        dirlight.shadow.camera.left = -500;
        dirlight.shadow.camera.right = 500;
        dirlight.shadow.camera.bottom = -200;
        dirlight.shadow.camera.top = 200;
        dirlight.shadow.camera.near = 0.5;    // default
        dirlight.shadow.camera.far = 5000;     // default

        return dirlight;
    }

    static spotlight() {
        // Spotlight 1
        var spotLight = new THREE.SpotLight(0xffffff, 1);
        spotLight.name = "CentralSpotLight";
        spotLight.position.set(15, 100, 35);
        spotLight.angle = Math.PI / 4;
        spotLight.penumbra = 0.05;
        spotLight.decay = 2;
        spotLight.distance = 250;
        spotLight.castShadow = true;
        spotLight.shadow.mapSize.width = 1024;
        spotLight.shadow.mapSize.height = 1024;
        spotLight.shadow.camera.near = 10;
        spotLight.shadow.camera.far = 200;

        return spotLight;
    }

}